<?php
class article extends access{
	public function __construct(){
		parent::__construct();
		$this->model=model('article');
	}
	public function index(){

	}
	public function add(){
		if($_POST){
			$_POST=mydtrim($_POST);
			extract($_POST);
			
			if(!$catid) msg(0,'分类不能为空');
			if(!$title) msg(0,'标题不能为空');
			if(!$content) msg(0,'内容不能为空');
			
			$addtime=time();
			$edittime=time();
			$userid=$this->userid;
			$username=$this->username;
			$editorid=$this->userid;
			$editor=$this->username;

			//$r=$this->db->insert('insert into article (catid,title,content,thumb,addtime,edittime,flag,userid,username,editorid,editor) values (:catid,:title,:content,:thumb,:addtime,:edittime,:flag,:userid,:username,:editorid,:editor)',['catid'=>$catid,'title'=>$title,'content'=>$content,'thumb'=>$thumb,'addtime'=>$addtime,'edittime'=>$edittime,'flag'=>$flag,'userid'=>$userid,'username'=>$username,'editorid'=>$editorid,'editor'=>$editor]);
			$r=$this->db->insert('insert into article %s',['catid'=>$catid,'title'=>$title,'content'=>$content,'thumb'=>$thumb,'addtime'=>$addtime,'edittime'=>$edittime,'flag'=>$flag,'userid'=>$userid,'username'=>$username,'editorid'=>$editorid,'editor'=>$editor]);

			msg($r);
		}
		$categorys=$this->db->column("select catid,catname from category order by catid asc");
		$category_select=form_select('catid',$categorys,0,'','请选择分类',0);
		include view();
	}
	public function mod(){
		extract($_GET);

		isset($itemid) or $itemid=0;
		$itemid=intval($itemid);
		if(!$itemid) msg(0);

		if($_POST){
			$_POST=mydtrim($_POST);
			extract($_POST);

			
			if(!$catid) msg(0,'分类不能为空');
			if(!$title) msg(0,'标题不能为空');
			if(!$content) msg(0,'内容不能为空');
			
			
			$edittime=time();
			$editorid=$this->userid;
			$editor=$this->username;

			//$r=$this->db->update('update article set catid=:catid,title=:title,content=:content,thumb=:thumb,edittime=:edittime,flag=:flag,editorid=:editorid,editor=:editor where itemid=:itemid',['catid'=>$catid,'title'=>$title,'content'=>$content,'thumb'=>$thumb,'edittime'=>$edittime,'flag'=>$flag,'editorid'=>$editorid,'editor'=>$editor,'itemid'=>$itemid]);
			$r=$this->db->update('update article %s where itemid=:itemid',['catid'=>$catid,'title'=>$title,'content'=>$content,'thumb'=>$thumb,'edittime'=>$edittime,'flag'=>$flag,'editorid'=>$editorid,'editor'=>$editor,'itemid'=>$itemid]);
			msg($r,'',$FORWORD);

		}

		$data=$this->db->find("select * from article where itemid=:itemid",['itemid'=>$itemid]);
		if(!$data) msg(0,'数据不存在');
		extract($data);

		$categorys=$this->db->column("select catid,catname from category order by catid asc");
		$category_select=form_select('catid',$categorys,$catid,'','请选择分类',0);


		include view();
	}
	public function lst(){
		extract($_GET);
		//
		$cond=['flag'=>"1,2"];
		$sql=' 1=1 and flag in(:flag) ';
		//title
		isset($title) or $title='';
		if($title){
			$cond['title']="%{$title}%";
			$sql.=" and title like :title ";
		}
		//catid
		isset($catid) or $catid=0;
		if($catid){
			$cond['catid']=$catid;
			$sql.=" and article.catid=:catid ";
		}
		//flag
		isset($flag) or $flag=0;
		if($flag){
			$cond['flag']=$flag;
			$sql.=" and flag=:flag ";
		}
		//总记录条数
		$total=$this->db->value("select count(*) from article inner join category on article.catid=category.catid where {$sql}",$cond);
		//实例化一个分页
		$pager=new page($total,20);

		$lst=$this->db->select("select article.*,category.catname from article inner join category on article.catid=category.catid where {$sql} order by itemid desc {$pager->limit}",$cond);


		foreach($lst as $k=>$v){
			$lst[$k]['flag']=myswitch($v['flag'],enums::$article_flag);
			$lst[$k]['addtime']=timeswitch($v['addtime']);
			$lst[$k]['edittime']=timeswitch($v['edittime']);
		}

		$categorys=$this->db->column("select catid,catname from category order by catid asc");
		$category_select=form_select('catid',$categorys,$catid,'','请选择分类',0);
		include view();
	}
	public function del(){
		extract($_GET);

		isset($itemid) or $itemid=0;
		$itemid=intval($itemid);
		if(!$itemid) msg(0);
		//设置flag为3
		$r=$this->db->update('update article set flag=3 where itemid=:itemid',['itemid'=>$itemid]);
		msg($r);
	}
}