<?php
class category extends access{
	public function __construct(){
		parent::__construct();
		$this->model=model('category');
	}
	public function index(){

	}
	public function add(){
		if($_POST){
			$_POST=mydtrim($_POST);
			extract($_POST);
			//catname
			if(!$catname) msg(0,'名称不能为空');
			
			//判断catname是否存在
			$e=$this->db->find('select * from category where catname=:catname',['catname'=>$catname]);
			if($e) msg(0,'名称已经存在');

		
			$r=$this->db->insert('insert into category (catname) values (:catname)',['catname'=>$catname]);
			msg($r);
		}
		include view();
	}
	public function mod(){
		extract($_GET);

		isset($catid) or $catid=0;
		$catid=intval($catid);
		if(!$catid) msg(0);

		if($_POST){
			$_POST=mydtrim($_POST);
			extract($_POST);

			$d=['catid'=>$catid,'catname'=>$catname];
			
			$r=$this->db->update('update category set catname=:catname where catid=:catid',$d);
			msg($r,'',$FORWORD);

		}

		$data=$this->db->find("select * from category where catid=:catid",['catid'=>$catid]);
		if(!$data) msg(0,'数据不存在');
		extract($data);


		include view();
	}
	public function lst(){
		extract($_GET);
		//
		$cond=[];
		$sql=' 1=1 ';
		//catname
		isset($catname) or $catname='';
		if($catname){
			$cond['catname']="%{$catname}%";
			$sql.=" and catname like :catname ";
		}
		
		//总记录条数
		$total=$this->db->value("select count(*) from category where {$sql}",$cond);
		//实例化一个分页
		$pager=new page($total,20);

		$lst=$this->db->select("select * from category where {$sql} order by catid desc {$pager->limit}",$cond);

		
		include view();
	}
	public function del(){
		extract($_GET);

		isset($catid) or $catid=0;
		$catid=intval($catid);
		if(!$catid) msg(0);
		//
		$r=$this->db->delete('delete from category where catid=:catid',['catid'=>$catid]);
		msg($r);
	}
}