<?php
class common extends controller{
	public function captcha(){
		// 建立一幅 100X30 的图像
		$im = imagecreate(100, 30);

		// 白色背景和蓝色文本
		$bg = imagecolorallocate($im, 255, 255, 255);
		$textcolor = imagecolorallocate($im, 0, 0, 255);

		// 把字符串写在图像左上角
		$str='';
		for ($i=0; $i < 4; $i++) { 
			$numbers=range(0,9);
			shuffle($numbers);
			$str.=$numbers[0];
		}
		$_SESSION['captcha']=$str;
		imagestring($im, 5, 30, 10, $str, $textcolor);

		// 输出图像
		header("Content-type: image/png");
		imagepng($im);
	}
}