<?php
class index extends access{
	public function __construct(){
		parent::__construct();
	}
	public function welcome(){
		

		include view();
	}
	public function safe(){
		$member_model=model('member');
		if($_POST){
			$old_password=trim($_POST['old_password']);
			$new_password=trim($_POST['new_password']);
			if(!$old_password or !$new_password) msg(0,'旧密码和新密码不能为空');
			//验证旧密码
			$d=$this->db->value('select password from member where userid=:userid',['userid'=>$this->userid]);
			if($member_model->encode_password($old_password)!=$d) msg(0,'旧密码不正确');

			//修改密码
			$new_password=$member_model->encode_password($new_password);
			$r=$this->db->update('update member set password=:password where userid=:userid',['userid'=>$this->userid,'password'=>$new_password]);
			msg($r);
		}

		include view();
	}
	public function logout(){
		unset($_SESSION['MY']);
		session_destroy();
		msg(1,'成功退出登录','welcome/login');
	}
}