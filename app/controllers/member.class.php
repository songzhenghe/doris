<?php
class member extends access{
	public function __construct(){
		parent::__construct();
		$this->model=model('member');
	}
	public function index(){

	}
	public function add(){
		if($_POST){
			$_POST=mydtrim($_POST);
			extract($_POST);
			//username
			if(!$username) msg(0,'账号不能为空');
			if(!$password) msg(0,'密码不能为空');
			//判断username是否被注册
			$e=$this->db->find('select * from member where username=:username',['username'=>$username]);
			if($e) msg(0,'账号已经被注册');

			$password=$this->model->encode_password($password);
			$r=$this->db->insert('insert into member (username,password,truename,telephone,qq,mobile,email,flag) values (:username,:password,:truename,:telephone,:qq,:mobile,:email,:flag)',['username'=>$username,'password'=>$password,'truename'=>$truename,'telephone'=>$telephone,'qq'=>$qq,'mobile'=>$mobile,'email'=>$email,'flag'=>$flag]);
			msg($r);
		}
		include view();
	}
	public function mod(){
		extract($_GET);

		isset($userid) or $userid=0;
		$userid=intval($userid);
		if(!$userid) msg(0);

		if($_POST){
			$_POST=mydtrim($_POST);
			extract($_POST);

			$password_sql='';
			$d=['userid'=>$userid,'truename'=>$truename,'telephone'=>$telephone,'qq'=>$qq,'mobile'=>$mobile,'email'=>$email,'flag'=>$flag];
			
			//如果填了密码则修改
			if($password){
				$password_sql=",password=:password";
				$d['password']=$this->model->encode_password($password);
			}
			$r=$this->db->update('update member set truename=:truename,telephone=:telephone,qq=:qq,mobile=:mobile,email=:email,flag=:flag where userid=:userid',$d);
			msg($r,'',$FORWORD);

		}

		$data=$this->db->find("select * from member where userid=:userid",['userid'=>$userid]);
		if(!$data) msg(0,'数据不存在');
		extract($data);


		include view();
	}
	public function lst(){
		extract($_GET);
		//默认显示正常和禁用的
		$cond=['flag'=>'1,2'];
		$sql=' 1=1 and flag in(:flag)';
		//username
		isset($username) or $username='';
		if($username){
			$cond['username']=$username;
			$sql.=" and username=:username ";
		}
		//flag
		isset($flag) or $flag=0;
		if($flag){
			$cond['flag']=$flag;
			$sql.=" and flag=:flag ";
		}
		//总记录条数
		$total=$this->db->value("select count(*) from member where {$sql}",$cond);
		//实例化一个分页
		$pager=new page($total,20);

		$lst=$this->db->select("select * from member where {$sql} order by userid desc {$pager->limit}",$cond);

		foreach($lst as $k=>$v){
			$lst[$k]['flag']=myswitch($v['flag'],enums::$member_flag);
		}
		include view();
	}
	public function del(){
		extract($_GET);

		isset($userid) or $userid=0;
		$userid=intval($userid);
		if(!$userid) msg(0);
		//设置flag为3
		$r=$this->db->update('update member set flag=3 where userid=:userid',['userid'=>$userid]);
		msg($r);
	}
}