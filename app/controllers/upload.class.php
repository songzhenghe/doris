<?php
class upload extends access{

	public function file(){
		extract($_GET);

		isset($callback) or $callback='';
		if(!$callback) msg(0,'lose callback');


		include view();
	}
	public function file_action(){
		if(empty($_FILES)){
			exit;
		}
		$config=array(
			'field_name'=>'file',
			'upload_path'=>'./upload/',
			'allow_type'=>array('.jpg','.png','.gif'),//数组
			'max_size'=>'50',
			'unit'=>'M',
			'israndname'=>true,
			'iscreatedir'=>true,
			'iscreatebydate'=>true,
			'isoverride'=>true
		);
		$upload=new sys_upload();
		$r=$upload->upload_instance($config);

		
		/*
"array(8) {
  ["origin_name"]=>
  string(14) "1157118606.jpg"
  ["new_name"]=>
  string(18) "15489266085072.jpg"
  ["file_type"]=>
  string(4) ".jpg"
  ["file_size"]=>
  int(122855)
  ["file_abspath"]=>
  string(20) "./upload/2019/01/31/"
  ["file_relpath"]=>
  string(11) "2019/01/31/"
  ["error_number"]=>
  int(0)
  ["error_message"]=>
  string(21) "文件上传成功！"
}
"
		*/
		if($r['error_number']<0){
			echo json_encode(array('code'=>0,'msg'=>$r['error_message'],'url'=>''));
		}else{
			echo json_encode(array('code'=>1,'msg'=>'success','url'=>'/upload/'.$r['file_relpath'].$r['new_name']));
		}

		

		exit;


	}
}