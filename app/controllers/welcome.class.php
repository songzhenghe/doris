<?php
class welcome extends controller{
	public function __construct(){
		parent::__construct();
	}
	public function index(){
		msg(1,'','welcome/login');
	}
	public function login(){
		if(isset($_SESSION['MY'])){
			msg(1,'','index/welcome');
		}
		$member_model=model('member');
		if($_POST){
			$username=trim($_POST['username']);
			$password=trim($_POST['password']);
			$captcha=trim($_POST['captcha']);
			if(!$username) msg(0,'账号不能为空');
			if(!$password) msg(0,'密码不能为空');
			if(!$captcha) msg(0,'验证码不能为空');

			//验证验证码
			if($captcha!=$_SESSION['captcha']) msg(0,'验证码不正确');
			//先查询一下账号是不是存在
			$user=$this->db->find("select * from member where username=:username and flag=1",['username'=>$username]);
			if(!$user) msg(0,'账号不存在');
			//密码对不对
			if($user['password']!=$member_model->encode_password($password)) msg(0,'密码不正确');
			//设置session
			$_SESSION['MY']=array(
				'USERID'=>$user['userid'],
				'USERNAME'=>$user['username'],
			);
			//跳转到首页
			msg(1,'登录成功，正在跳转...','index/welcome');
		}
		include view();
	}
}