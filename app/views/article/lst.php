<!DOCTYPE html>
<html>
<head>
	<title>网站内空管理系统</title>
	<?php include view('common/cssjs');?>
</head>
<body>
<?php include view('common/header');?>
<?php include view('common/left');?>

<div style="float:left;">
<form>
<input type="hidden" name="r" value="<?php echo $_GET['r'];?>">
<table width="100%" border="0" class="table table-bordered">
  <tr>
    <td>分类：<?php echo $category_select;?> 标题：<?php echo form_input_text('title',$title);?> 状态：<?php echo form_select('flag',enums::$article_flag,$flag,'','请选择',0);?></td>
  </tr>
  <tr>
    <td align="center"><input type="submit" name="submit" value="搜索"> <input type="button" value="重置" onClick="location.href='?r=article/lst';" ></td>
  </tr>
</table>
</form>
<table width="100%" border="0" class="table table-bordered">
<?php if(count($lst)){?>
  <tr>
    <td>ID</td>
    <td>分类</td>
    <td>标题</td>
    <td>添加时间</td>
    <td>编辑时间</td>
    <td>状态</td>
    <td>编辑</td>
    <td>删除</td>
  </tr>
  <?php foreach($lst as $k=>$v){?>
  <tr>
    <td><?php echo $v['itemid'];?></td>
    <td><?php echo $v['catname'];?></td>
    <td><?php echo $v['title'];?></td>
    <td><?php echo $v['addtime'];?></td>
    <td><?php echo $v['edittime'];?></td>
    <td><?php echo $v['flag'];?></td>
    <td><a href="?r=article/mod&itemid=<?php echo $v['itemid'];?>" class="">编辑</a></td>
    <td><a onclick="return confirm('确定要删除吗?');" href="?r=article/del&itemid=<?php echo $v['itemid'];?>" class="">删除</a></td>
  </tr>
  <?php
  }
  ?>
  <tr>
    <td colspan="6"><?php echo $pager->fpage();?></td>
  </tr>
  <?php }else{?>
  <tr>
    <td colspan="6">暂无数据</td>
  </tr>
  <?php }?>
</table>

</div>

<?php include view('common/footer');?>
</body>
</html>