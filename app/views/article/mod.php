<!DOCTYPE html>
<html>
<head>
	<title>网站内空管理系统</title>
	<?php include view('common/cssjs');?>
</head>
<body>
<?php include view('common/header');?>
<?php include view('common/left');?>

<div style="float:left;width:900px;">
<form method="post">
  <input type="hidden" name="FORWORD" value="<?php echo $this->FORWORD?>">
<table width="100%" border="0" class="table table-bordered">
  <tr>
    <td align="right">分类*</td>
    <td><?php echo $category_select;?></td>
  </tr>
  <tr>
    <td align="right">标题*</td>
    <td><?php echo form_input_text('title',$title);?></td>
  </tr>
  <tr>
    <td align="right">内容*</td>
    <td><?php echo form_textarea('content',$content);?></td>
  </tr>
  <tr>
    <td align="right">缩略图</td>
    <td><?php echo form_input_text('thumb',$thumb);?><a class="example1" href="?r=upload/file&callback=abc">[上传]</a><a href="javascript:clear_abc();">[清空]</a></td>
  </tr>
  <tr>
    <td align="right">状态</td>
    <td><?php echo form_radio('flag',enums::$article_flag,$flag);?></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input type="submit" value="提交" name="submit"></td>
    </tr>
</table>
</form>
</div>

<script type="text/javascript">
  $('textarea[name=content]').ckeditor();
  $('.example1').colorbox({width:'600px',height:'400px',iframe:true});
  function abc(data){
    console.log(data);
    $('input[name=thumb]').val(data.url);
    $.colorbox.close();
  }
  function clear_abc(){
    $('input[name=thumb]').val('');
    $.colorbox.close();
  }
</script>

<?php include view('common/footer');?>
</body>
</html>