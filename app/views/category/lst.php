<!DOCTYPE html>
<html>
<head>
	<title>网站内空管理系统</title>
	<?php include view('common/cssjs');?>
</head>
<body>
<?php include view('common/header');?>
<?php include view('common/left');?>

<div style="float:left;">
<form>
<input type="hidden" name="r" value="<?php echo $_GET['r'];?>">
<table width="100%" border="0" class="table table-bordered">
  <tr>
    <td>名称：<?php echo form_input_text('catname',$catname);?> </td>
  </tr>
  <tr>
    <td align="center"><input type="submit" name="submit" value="搜索"> <input type="button" value="重置" onClick="location.href='?r=category/lst';" ></td>
  </tr>
</table>
</form>
<table width="100%" border="0" class="table table-bordered">
<?php if(count($lst)){?>
  <tr>
    <td>ID</td>
    <td>名称</td>
    <td>编辑</td>
    <td>删除</td>
  </tr>
  <?php foreach($lst as $k=>$v){?>
  <tr>
    <td><?php echo $v['catid'];?></td>
    <td><?php echo $v['catname'];?></td>
    <td><a href="?r=category/mod&catid=<?php echo $v['catid'];?>" class="">编辑</a></td>
    <td><a onclick="return confirm('确定要删除吗?');" href="?r=category/del&catid=<?php echo $v['catid'];?>" class="">删除</a></td>
  </tr>
  <?php
  }
  ?>
  <tr>
    <td colspan="6"><?php echo $pager->fpage();?></td>
  </tr>
  <?php }else{?>
  <tr>
    <td colspan="6">暂无数据</td>
  </tr>
  <?php }?>
</table>

</div>

<?php include view('common/footer');?>
</body>
</html>