<!DOCTYPE html>
<html>
<head>
	<title>网站内空管理系统</title>
	<?php include view('common/cssjs');?>
</head>
<body>
<?php include view('common/header');?>
<?php include view('common/left');?>

<div style="float:left;">
<form method="post">
<table width="100%" border="0" class="table table-bordered">
  <tr>
    <td align="right">账号*</td>
    <td><?php echo form_input_text('username','');?></td>
  </tr>
  <tr>
    <td align="right">密码*</td>
    <td><?php echo form_input_text('password','');?></td>
  </tr>
  <tr>
    <td align="right">真实姓名</td>
    <td><?php echo form_input_text('truename','');?></td>
  </tr>
  <tr>
    <td align="right">电话</td>
    <td><?php echo form_input_text('telephone','');?></td>
  </tr>
  <tr>
    <td align="right">qq</td>
    <td><?php echo form_input_text('qq','');?></td>
  </tr>
  <tr>
    <td align="right">手机</td>
    <td><?php echo form_input_text('mobile','');?></td>
  </tr>
  <tr>
    <td align="right">email</td>
    <td><?php echo form_input_text('email','');?></td>
  </tr>
  <tr>
    <td align="right">状态</td>
    <td><?php echo form_radio('flag',enums::$member_flag,1);?></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input type="submit" value="提交" name="submit"></td>
    </tr>
</table>
</form>
</div>

<?php include view('common/footer');?>
</body>
</html>