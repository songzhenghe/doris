<!DOCTYPE html>
<html>
<head>
	<title>网站内空管理系统</title>
	<?php include view('common/cssjs');?>
</head>
<body>
<?php include view('common/header');?>
<?php include view('common/left');?>

<div style="float:left;">
<form method="post">
  <input type="hidden" name="FORWORD" value="<?php echo $this->FORWORD?>">
<table width="100%" border="0" class="table table-bordered">
  <tr>
    <td align="right">账号*</td>
    <td><?php echo $username;?></td>
  </tr>
  <tr>
    <td align="right">密码*</td>
    <td><?php echo form_input_text('password','');?></td>
  </tr>
  <tr>
    <td align="right">真实姓名</td>
    <td><?php echo form_input_text('truename',$truename);?></td>
  </tr>
  <tr>
    <td align="right">电话</td>
    <td><?php echo form_input_text('telephone',$telephone);?></td>
  </tr>
  <tr>
    <td align="right">qq</td>
    <td><?php echo form_input_text('qq',$qq);?></td>
  </tr>
  <tr>
    <td align="right">手机</td>
    <td><?php echo form_input_text('mobile',$mobile);?></td>
  </tr>
  <tr>
    <td align="right">email</td>
    <td><?php echo form_input_text('email',$email);?></td>
  </tr>
  <tr>
    <td align="right">状态</td>
    <td><?php echo form_radio('flag',enums::$member_flag,$flag);?></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input type="submit" value="提交" name="submit"></td>
    </tr>
</table>
</form>
</div>

<?php include view('common/footer');?>
</body>
</html>