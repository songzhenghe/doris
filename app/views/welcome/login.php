<!DOCTYPE html>
<html>
<head>
	<title>登录</title>
    <?php include view('common/cssjs');?>
</head>
<body>
	<h2 style="text-align: center;">CMS后台登录</h2>
<form method="post">
<table width="100%" border="0" class="table table-bordered">
  <tr>
    <td align="right">账号</td>
    <td align="left"><input type="text" name="username" value=""></td>
  </tr>
  <tr>
    <td align="right">密码</td>
    <td align="left"><input type="password" name="password" value=""></td>
  </tr>
  <tr>
    <td align="right">验证码</td>
    <td align="left"><input type="text" name="captcha" value=""><img id="captcha" src="?r=common/captcha"></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input type="submit" name="submit" value="登录"></td>
  </tr>
</table>
</form>

<script type="text/javascript">
	$(document).ready(function(){
		$('#captcha').click(function(){
			$(this).attr('src',"?r=common/captcha&i="+Math.random());
		});
	});
</script>

</body>
</html>