<?php
$config=array();
//基本配置
$config['base']=array(
	//网站域名
	'base_url'=>'doris.com',
);
$config['db']=array(
	//dsn
	'dsn'=>'mysql:host=127.0.0.1;dbname=doris;port=3306',
	//数据库用户名
	'username'=>'root',
	//数据库密码
	'password'=>'root',
	//数据库编码
	'char_set'=>'utf8',

);

return $config;