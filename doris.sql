-- phpMyAdmin SQL Dump
-- version phpStudy 2014
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2019 �?01 �?31 �?09:36
-- 服务器版本: 5.5.53
-- PHP 版本: 5.6.27

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `doris`
--

-- --------------------------------------------------------

--
-- 表的结构 `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `itemid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(10) unsigned NOT NULL,
  `title` varchar(500) NOT NULL,
  `content` text NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `addtime` int(10) unsigned NOT NULL,
  `edittime` int(10) unsigned NOT NULL,
  `flag` tinyint(3) unsigned NOT NULL,
  `userid` int(10) unsigned NOT NULL,
  `username` varchar(30) NOT NULL,
  `editorid` int(10) unsigned NOT NULL,
  `editor` varchar(30) NOT NULL,
  PRIMARY KEY (`itemid`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `article`
--

INSERT INTO `article` (`itemid`, `catid`, `title`, `content`, `thumb`, `addtime`, `edittime`, `flag`, `userid`, `username`, `editorid`, `editor`) VALUES
(1, 1, '111a', '<p>222a</p>', '333a', 1548838863, 1548924495, 1, 1, 'admin', 1, 'admin'),
(3, 1, '“南方养老金支援东北”，没你想象的那么不公平', '<p><strong>要点 | 速读</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><em>1</em>发达省份一般是人口流入大省，工作人口多，而欠发达省份一般是人口流出大省，交社保的人少，领社保的老人相对多。流动人口很难在发达地区安家落户，结果他们在发达地区交社保，却很难在当地领社保，发达省份的养老金结余自然高。</li>\r\n	<li><em>2</em>贾康建议养老金全国统筹，富省帮助穷省，也是为降低企业社保费率提供路径&mdash;&mdash;试想，如果一个省本身养老金已经收不抵支，那它哪有动力降低社保费率呢？</li>\r\n	<li><em>3</em>为何我国的养老金全国统筹一直进展缓慢？这既有技术和管理的因素，也有各级统筹部门的利益博弈在里面。</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>文 | 刘文昭</strong></p>\r\n\r\n<p>养老金够不够发，每个人都关心。最近，中国财政学会副会长贾康&ldquo;把南方多年的滚存结余调到东北就燃眉之急&rdquo;的建议，让网友炸开了锅。&ldquo;南方社保资金结余多，是因为年轻人多交的多，以后我们怎么办&rdquo;，这不是&ldquo;自己账上有余钱就被要求支援隔壁老王&rdquo;，不少媒体也认为这个办法太不公平。</p>\r\n\r\n<p><strong>养老金&ldquo;南钱北调&rdquo;已经有了，这种调剂有其公平意义</strong></p>\r\n\r\n<p>贾康的建议是在12日某经济论坛上提出的，他的演讲是关于整个财税改革的前瞻，说到养老金，他建议尽快把全社会基本养老的统筹机制提升到全国是一个蓄水池，如此南方多年的滚存结余就可以调到东北救燃眉之急。</p>\r\n\r\n<p>没想到，只占整个演讲极小部分的养老金&ldquo;南钱北调&rdquo;，引发轩然大波，南方网友骂声一片。</p>\r\n\r\n<p>其实，为解决部分省份养老金收支缺口问题，&ldquo;南钱北调&rdquo;已经开始了。2018年7月1日，养老保险基金中央调剂制度正式实施，中央调剂基金由各省份养老保险基金上解的资金构成，上解比例初步定为3%。</p>\r\n\r\n<p><img src="https://inews.gtimg.com/newsapp_bt/0/3967300947/1000" /></p>\r\n\r\n<p>在很多学者看来，这是养老金国家统筹的第一步，表明政府要对各省的养老金统一调配，&ldquo;劫富济贫&rdquo;了。可能是因为制度过于抽象，当时很少有人关注。</p>\r\n\r\n<p>一些南方网友会说，这也不太公平了，我们账上的余钱为什么要给北方？以后他们用什么还？</p>\r\n\r\n<p>这说明很多人还不清楚本省养老金结余是怎么来的。先来看两组数据，根据2016年社保报告，当年年末基本养老保险累计结存4万多亿元。养老金总量问题不大，但结构有问题。</p>\r\n\r\n<p>表现在各省结余差别很大，广东为7258亿，黑龙江为负232亿。河北、吉林、内蒙古、湖北和青海也出现了当期收不抵支的状况。</p>\r\n\r\n<p><img src="https://inews.gtimg.com/newsapp_bt/0/4264702623/1000" /></p>\r\n\r\n<p>另一组数字是抚养比，它是参保职工人数与领取养老保险待遇人数的比值，可以显示出养老保险基金的压力。</p>\r\n\r\n<p>不同省份之间差别依然较大，最高的是广东超过了9.25比1，有的省份抚养比却不到2比1，比如四川1.75比1，最低的黑龙江不到1.3比1。</p>\r\n\r\n<p>为什么发达的南方省份抚养比和养老金结余高，欠发达地区相对较低呢？当然不是因为发达省份的人生孩子多，而是发达省份一般是人口流入大省，工作人口多，而欠发达省份一般是人口流出大省，交社保的人少，领社保的老人相对多。</p>\r\n\r\n<p><img src="https://inews.gtimg.com/newsapp_bt/0/7413832429/1000" /></p>\r\n\r\n<p>由于户籍制度的限制，流动人口很难在发达地区安家落户，结果他们成长、读书的成本在家乡，在发达地区交社保，却很难在当地领社保，发达省份坐享劳动红利，养老金结余自然高。</p>\r\n\r\n<p>这种现状持续下去，才是不公。</p>\r\n\r\n<p>面对贾康建议引发的争论，世界社保研究中心主任郑秉文即认为，&ldquo;（外出务工者）他们父母和子女成了留守老人和留守儿童，需要发养老金呀，但他们却把钱交给了广东，用中央调剂制度把一部分钱转移支付给黑龙江，下一代养活上一代，这也是公平的。&rdquo;</p>\r\n\r\n<p><strong>南北省份养老金结余差距大，背后是我国养老金尚未全国统筹</strong></p>\r\n\r\n<p>也许南方网友还会说，那欠发达省份可以将国有资产注入社保，或者努力发展经济，有了就业，养老金自然充足。</p>\r\n\r\n<p>首先，贾康在回应中表示，他的建议和&ldquo;以国有企业资产收益支持社会保障体系运转&rdquo;的建议，并不发生矛盾，可以、也应当同时使用，相辅相成。</p>\r\n\r\n<p><img src="https://inews.gtimg.com/newsapp_bt/0/7413838859/1000" /><em>贾康回复网友评论</em></p>\r\n\r\n<p>实际上，2017年公布的《划转部分国有资本充实社保基金实施方案》也明确，从当年开始，选择部分中央企业和部分省份试点，统一划转企业国有股权的10%充实社保基金。</p>\r\n\r\n<p>其次，我国各省养老金结余差距巨大，和养老金的统筹水平有很大关系。</p>\r\n\r\n<p>要知道，我国的社保制度在建立之初是交由各地政府自行探索的，结果导致了各地养老金极为&ldquo;碎片化&rdquo;。</p>\r\n\r\n<p>养老金有结余的，可以在社保费率上更灵活，对企业更有吸引力；养老金紧张的，不愿少收一分，企业就更不愿去。因果循环，各地社保形成了&ldquo;旱得旱死，涝得涝死&rdquo;的僵局。</p>\r\n\r\n<p><img src="https://inews.gtimg.com/newsapp_bt/0/7413866163/1000" /></p>\r\n\r\n<p>据财经网报道，中国经济改革研究基金会理事长宋晓梧多次举的一个例子是，在黑龙江投资一个招收1万名工人的企业，向每名工人发放平均每月5000元的工资，一年的工资成本是6亿元。</p>\r\n\r\n<p>公司在黑龙江缴纳养老金的比例是22%，而在东南某省只需缴纳13%，两者相差9%，即企业每年可以省下5400万元，那为什么要在黑龙江投资？</p>\r\n\r\n<p>目前，企业的社保缴费负担沉重。2018年12月，清华大学经济管理学院弗里曼讲席教授白重恩指出，社保缴费在中国企业利润中的占比为48%，而德国这一比例为21%。</p>\r\n\r\n<p>他认为，造成中国企业税费负担高的正是社保缴费。相对于其他社保缴费，养老保险缴费率最高，&ldquo;降低养老保险缴费，对中国经济高质量发展而言，可谓是盘活棋局的重要举措。&rdquo;</p>\r\n\r\n<p>贾康建议养老金全国统筹，富省帮助穷省，也是为降低企业社保费率提供可行的路径&mdash;&mdash;试想，如果一个省本身养老金已经收不抵支，它哪有动力降低社保费率呢？</p>\r\n\r\n<p>其实，我国现有养老金统筹体系缴费标准高（世界范围内几乎为最高），统筹层次低，问题重重早已饱受诟病，加快养老金全国统筹，已是学界和政策界的共识。而养老金在国家范围内统收统支，也是主要国家的通行做法。</p>\r\n\r\n<p><strong>养老金全国统筹需要考虑各方利益，但不能一直拖下去</strong></p>\r\n\r\n<p>养老金全国统筹，富裕省份的人也不用担心自己将来领不到钱。如贾康所说，不论怎样统筹，是在市级、省级，还是提升到全国，缴费者在退休后领取时的受益标准（领到多少钱）都是要按规则执行、受法律保证的，并不因为他的倡议而改变。每位具体的养老金领取人，拿到的这份钱仍是按法定标准，既不会多，也不会少。</p>\r\n\r\n<p>养老金全国统筹的好处很明显，不仅可以促进人员自由流动，还可以提升养老金管理水平，利其保值增值，眼下则有望降低企业税率。为什么我国的养老金全国统筹一直进展缓慢呢？这既有技术和管理的因素，也有各级统筹部门的利益博弈。</p>\r\n\r\n<p>中国人民大学公共管理学院教授李珍指出，每个地方政府都希望本地企业的负担轻以促进经济的增长，减轻社保负担也是招商引资的一个有利条件。但在财政分灶吃饭的前提下，统筹层次提高到哪一级，哪一级政府实际就成为最终的财务负责人。</p>\r\n\r\n<p>养老金积累的多的省份，资金就存在当地银行，对当地金融业和经济发展有很大的支持，这也是为什么有结余大省的社保人员表示，他们不反对全国统筹，前提是将现有结余留在省内，新制度执行后的基金再进行全国统筹。</p>\r\n\r\n<p>贾康在回应网友时更是激愤的表示，一些技术层面的顾虑，可说基本都是借口，实质是原已形成的几十万人管理缴费的&ldquo;利益固化的藩篱&rdquo;。</p>\r\n\r\n<p>改革总要触动既得利益，虽然养老金全国统筹也并非完美，也要防止地方虚报退休人数，降低征缴积极性等道德风险，但这些问题并非无药可解，更不该成为一拖再拖的借口。</p>', '/upload/2019/01/31/15489271598752.jpg', 1548924462, 1548927162, 1, 1, 'admin', 1, 'admin');

-- --------------------------------------------------------

--
-- 表的结构 `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `catid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catname` varchar(100) NOT NULL,
  PRIMARY KEY (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `category`
--

INSERT INTO `category` (`catid`, `catname`) VALUES
(1, '中国'),
(2, '美国'),
(3, '俄国a');

-- --------------------------------------------------------

--
-- 表的结构 `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `userid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` char(32) NOT NULL,
  `truename` varchar(20) NOT NULL,
  `telephone` varchar(50) NOT NULL,
  `qq` varchar(20) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `flag` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`userid`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `member`
--

INSERT INTO `member` (`userid`, `username`, `password`, `truename`, `telephone`, `qq`, `mobile`, `email`, `flag`) VALUES
(1, 'admin', '827ccb0eea8a706c4c34a16891f84e7b', '', '', '', '', '', 1),
(2, 'jack', '827ccb0eea8a706c4c34a16891f84e7b', '杰克', '12345', '12345', '15123456789', 'jack@126.com', 1),
(3, 'mary', '827ccb0eea8a706c4c34a16891f84e7b', '玛丽', '1', '2', '3', '4', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
