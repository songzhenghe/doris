<?php
//调试函数
function p($var){
	echo '<pre>';
	var_dump($var);
	echo '</pre>';
}
//自动加载类文件
function __autoload($class){
	$a=BASEPATH.$class.'.class.php';
	$b=APPPATH.'core/'.$class.'.class.php';
	if(file_exists($a)){
		include_once $a;
	}
	if(file_exists($b)){
		include_once $b;
	}
}
//路由没有找到时的提示
function error_404($message=''){
	echo '没有找到操作'.$message;
	exit;
}
//加载视图
function view($name=''){
	if(!$name) $name=MODULE.'/'.ACTION;
	return APPPATH.'views/'.$name.'.php';
}
//加载模型
function model($name){
	$class=$name.'_model';
	require_once APPPATH.'models/'.$class.'.class.php';
	return new $class();
}
function msg($r,$msg='',$url=''){
	if($r and !$msg) $msg='操作成功';
	if(!$r and !$msg) $msg='操作失败';

	echo "<h2 style='width:400px;height:30px;line-height:30px;text-align:center;background:skinblue;border:1px solid green;margin:0 auto;'>{$msg}</h2>";
	if(!$r) {
		echo "<script>setTimeout(function(){history.back();},3000);</script>";
	}
	if($r and $url){
		if(strpos($url, 'http')!==false){
		}else{
			$url='?r='.$url;
		}
		echo "<script>setTimeout(function(){location.href='{$url}';},1000);</script>";
	}
	if($r and !$url){
		echo "<script>setTimeout(function(){history.back();},3000);</script>";
	}
	exit;
}
function mydtrim($arr){
	if(is_array($arr)){
		foreach($arr as $k=>$v){
			$arr[$k]=mydtrim($v);
		}
	}else{
		$arr=trim($arr);
	}
	return $arr;
}
function myswitch($kvalue,$array){
	return  isset($array[$kvalue])?$array[$kvalue]:'';
}
function timeswitch($ts,$format='Y-m-d',$default=''){
    return $ts?date($format,$ts):$default;
}
function form_input_text($name,$value='',$id='',$class='col-xs-4'){
    if(!$id) $id=$name;
    return <<<st
<input type="text" class="{$class}" name="{$name}" value="{$value}" id="{$name}">
st;

}
function form_input_hidden($name,$value='',$id='',$class=''){
    return <<<st
<input type="hidden" name="{$name}" value="{$value}" id="{$id}" class="{$class}">
st;
}
function form_textarea($name,$value='',$cols=30,$rows=10,$id='',$class=''){
    if(!$id) $id=$name;
    return <<<st
<textarea name="{$name}" id="{$id}" cols="{$cols}" rows="{$rows}">{$value}</textarea>
st;

}
//echo form_radio('tag',array(1=>'显示',0=>'隐藏',2=>'很好'),1);
function form_radio($name,$array,$checked='-1'){
	$string='';
   foreach($array as $key=>$value){
		if($checked==$key){
			$c="checked='checked'";
		}else{
			$c='';
		}
		$string.=<<<st
\n\r<label><input type="radio" name="{$name}" value="{$key}" {$c} /> {$value}</label>&nbsp;
st;
   }
   return $string."\r\n";
}
//echo form_checkbox('mycheckbox',array(0=>'音乐',1=>'体育',2=>'政治'),array(1,2));
function form_checkbox($name,$array,$checked=array()){
	$string='';
	foreach($array as $key=>$value){
		if(in_array($key,$checked)){
			$c="checked='checked'";
		}else{
			$c='';
		}
		$string.=<<<st
\n\r<label><input type="checkbox" name="{$name}" value="{$key}" {$c} /> {$value}</label>&nbsp;
st;
   }
   return $string."\r\n";
}
//echo form_select('catid',array(0=>'第0个',1=>'第一个',2=>'第二个'),2);
function form_select($name, $sarray, $selected, $extend = '', $title = '', $ov = '', $key = 1, $abs = 0){
	$select = '<select name="'.$name.'" '.$extend.'>';
	if($title) $select .= '<option value="'.$ov.'">'.$title.'</option>';
	foreach($sarray as $k=>$v) {
		if(!$v) continue;
		$_selected = ($abs ? ($key ? $k : $v) === $selected : ($key ? $k : $v) == $selected) ? ' selected=selected' : '';
		$select .= '<option value="'.($key ? $k : $v).'"'.$_selected.'>'.$v.'</option>';
	}	
	$select .= '</select>';
	return $select;
}