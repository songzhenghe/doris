<?php
defined('BASEPATH') or exit('access denied');
//设置网页编码
header("Content-Type:text/html;charset=UTF-8");
//错误日志
if(defined('DEBUG') and DEBUG){
	error_reporting(E_ALL);
	ini_set('display_errors', 'On');
	ini_set('log_errors', 'Off');
}else{
	error_reporting(0);
	ini_set('display_errors', 'Off');
	ini_set('log_errors', 'On');
	ini_set('error_log','./log/error_log.txt');
}
//设置时区
date_default_timezone_set('Asia/Shanghai');
//包含配置文件
$CFG=require dirname(__FILE__).'/../config.inc.php';
//开启session
session_start();

//处理路由
$route=explode('/',trim($_GET['r'],'/'));
$route=array_map('trim',$route);
$route=array_map('strtolower',$route);
(isset($route[0]) && $route[0]) or error_404('route:0'); 
(isset($route[1]) && $route[1]) or error_404('route:1'); 

define('MODULE',$route[0]);
define('ACTION',$route[1]);
//引入自定义函数文件
require BASEPATH.'functions.inc.php';
//引入项目自定义函数文件
require APPPATH.'functions.inc.php';
//包含controller
require_once BASEPATH.'controller.class.php';

//调用app的控制器
$class=APPPATH.'controllers/'.$route[0].'.class.php';
file_exists($class) or error_404('file error');
require_once $class;

//排除一些魔术方法
$deny_list=array('__construct','__destruct','__call','__callStatic','__get','__set','__isset','__unset','__sleep','__wakeup','__toString','__invoke','__set_state','__clone');
if(in_array($route[1],$deny_list)) error_404();

//初始化数据库
require_once BASEPATH.'db.class.php';
$db=new db();
//包含model
require_once BASEPATH.'model.class.php';
//实例化类
$object=new $route[0]();

//利用反射类判断方法是否可以调用
$ref=new ReflectionMethod($object,$route[1]);
if(!$ref->isPublic()) error_404();

//调用类下的方法
$object->$route[1]();